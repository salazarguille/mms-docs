# Importacion de Datos

## Introduccion

**MMS** permite la creacion de datos en forma masiva, a partir, de archivos excel (xls, o xlsx), y de archivos de texto.

Cabe aclarar algunos conceptos para cada tipo de archivos:


### Archivos Excel

*	Los formatos soportados son xls, y xlsx.
*	Para cada tipo de datos a importar, la hoja del excel debe tener un formato particular. Para conocer dicho formato se puede bajar un excel como ejemplo, previamente seleccionando el tipo a importar del combo.
*	Si la hoja de excel posee un encabezado, se debe tildar el checkbox "_Tiene Header?_"
*	Se debe definir el numero de hoja desde la cual se van a proceder 

### Archivos de Textos

*	El formato soportado es txt.
*	Cada columna en cada registro del archivo debe estar separada por "_,_" (_coma_).

## Importacion de Menues

Esta opcion esta habilitada solamente para usuarios con permisos de administrador de la aplicacion.

## Importacion de Tipos de Proveedores

El formato del archivo a utilizar en la importacion de tipos de proveedores debe ser la siguiente:

*	Nombre de la Empresa (tal cual aparece en el perfil de la empresa).
*	Nombre del tipo de proveedor.
*	Descripcion del tipo de proveedor.

## Importacion de Tipos de Herramientas

El formato del archivo a utilizar en la importacion de tipos de herramientas debe ser la siguiente:

*	Nombre de la Empresa (tal cual aparece en el perfil de la empresa).
*	Nombre del tipo de herramienta.
*	Descripcion del tipo de herramienta.
