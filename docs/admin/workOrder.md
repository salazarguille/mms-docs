# Gestion de Rutinas

**MMS** ofrece las administracion de las ordenes de trabajo que su empresa asigna a sus empleados, permitiendo tener una trazabilidad durante todo el ciclo de las mismas.
Esto le permite a su empresa poder ver en un momento determinado en que estado se encuentra cada una de las ordenes de trabajo de su empresa, verificar si hay retrasos, incumplimientos, entre otros conceptos.

## Rutinas

### Introduccion a las Rutinas

Las rutinas son definiciones de trabajo asociadas a un documento que detalla el trabajo a realizar en la rutina. Estas se utilizan para definir un trabajo periodico, y da la posibilidad de poder generar ordenes de trabajo.

### Listado de Rutinas

Como en otras entidades, **MMS** ofrece un listado de rutinas asociadas a su empresa, en la cual se pueden visualizar los siguientes datos:

*	Titulo.
*	Frecuencia.
*	Horas Estimadas.
*	Requerimiento.

#### Acciones Disponibles

Como en todos los listados de **MMS** se ofrecen distintas acciones sobre cada registro. En el caso de las rutinas estan disponibles las siguientes:

##### Habilitar/Deshabilitar una Rutina

Cada rutina puede habilitarse o deshabilitarse para no permitir la generacion de ordenes de trabajo, o porque se debe modificar su informacion.

##### Generar Ordenes de Trabajo

**MMS** permite facilitar la creacion de ordenes de trabajo repetitivas a partir de las rutinas. Para generar ordenes de trabajo a partir de una rutina, debe:

*	Presionar el boton de _Generar Ordenes de Trabajo_ sobre la rutina que desea en el listado de rutinas.
*	Completar el formulario para generar las ordenes de trabajo. Los datos solicitados en dicho formulario son: Usuario asignado, fecha y hora estimada de comienzo, y el documento de rutina.

  
`Las ordenes de trabajo se generaran con la frecuencia definida en el formulario. La cantidad de meses hasta la cual se generar las ordenes de trabajo es una configuracion de la empresa. Por defecto es 1 (un) mes.` 

#### Editar una Rutina

Para editar una rutina, debe:

*	Acceder al listado de rutinas (ver [Listado de Rutinas](../admin/workOrder.md#listado-de-rutinas "Ver Listado de Rutinas")).
*	Presionar sobre el icono de edicion en el registro de la rutina que desee.

#### Eliminar una Rutina

Para eliminar una rutina, debe:

*	Acceder al listado de rutinas (ver [Listado de Rutinas](../admin/workOrder.md#listado-de-rutinas "Ver Listado de Rutinas")).
*	Presionar sobre el icono de eliminacion ("_X_") en el registro de la rutina que desee.

### Creacion de Rutinas

Para acceder al formulario de creacion de rutinas, debe:

*	Acceder al listado de rutinas (ver [Listado de Rutinas](../admin/workOrder.md#listado-de-rutinas "Ver Listado de Rutinas")).
*	Presionar el boton debajo del listado con titulo _Crear Rutina_.
*	Completar el formulario de creacion de rutina.

# Gestion de Ordenes de Trabajo

## Listado de Ordenes de Trabajo

**MMS** ofrece acceder al listado de las ordenes de trabajo de su empresa. Para realizar esto, debe:

*	Acceder al menu principal llamado _Admin Ordenes de Trabajo_.
*	Seleccionar la opcion _Listado Ordenes de Trabajo_.

Ahora se puede visualizar el listado de ordenes de trabajo de su empresa. Podra visualizar un grafico que interpreta la cantidad de horas entre:

*	La estimacion realizada.
*	Las cantidad de horas desde que se creo hasta ese momento.
*	La cantidad de horas trabajadas por el usuario (en caso de aplicar).

### Acciones Disponibles

#### Estimar Orden de Trabajo

Se puede estimar las horas en resolver una orden de trabajo que aun esta pendiente de estimacion.

#### Estimar y Asignar Orden de Trabajo

Se puede realizar dos acciones completando un solo formulario. Es el caso de la estimacion y asignacion de una orden de trabajo para facilitar la operacion.

#### Cancelar Orden de Trabajo

La cancelacion de una orden de trabajo termina el ciclo de vida de la misma, no permitiendo que esta pueda trabajarse, o reabrirla.

#### Visualizar Orden de Trabajo

Se puede visualizar toda la informacion asociada a una orden de trabajo.

#### Editar Orden de Trabajo


#### Eliminar Orden de Trabajo

Presionando el boton de eliminacion (_X_), borra una orden de trabajo, sin posibilidad de ser reestablecida.

## Monitor de Ordenes de Trabajo

Para acceder al monitor de ordenes de trabajo, debe:

*	Acceder al menu principal llamado _Admin Ordenes de Trabajo_.
*	Seleccionar la opcion _Monitor de Ordenes de Trabajo_.

Aqui se puede visualizar diversos graficos basados en las ordenes de trabajo de la empresa.

## Ordenes de Trabajo Asignadas a Mi.

Para acceder al listado de ordenes de trabajo asignadas a mi, debe:

*	Acceder al menu principal llamado _Admin Ordenes de Trabajo_.
*	Seleccionar la opcion _Orden de Trabajo Asignadas a Mi_.

El listado visualizado es similar al listado de ordenes de trabajo, simplemente que se filtraron por el usuario logueado.